/*
 * jQuery Basic Table
 * Author: Jerry Low
 */
(function (e) {
    e.fn.basictable = function (t) {
        var n = function (t, n) {
                var i = [];
                n.tableWrap && t.wrap('<div class="bt-wrapper"></div>');
                var s = "";
                t.find("thead tr th").length ? s = "thead th" : t.find("tbody tr th").length ? s = "tbody tr th" : t.find("th").length ? s = "tr:first th" : s = "tr:first td", e.each(t.find(s), function () {
                    var t = e(this),
                        n = parseInt(t.attr("colspan"), 10) || 1,
                        r = t.closest("tr").index();
                    i[r] || (i[r] = []);
                    for (var s = 0; s < n; s++) i[r].push(t)
                }), e.each(t.find("tbody tr"), function () {
                    r(e(this), i, n)
                }), e.each(t.find("tfoot tr"), function () {
                    r(e(this), i, n)
                })
            },
            r = function (t, n, r) {
                t.children().each(function () {
                    var t = e(this);
                    if (t.html() !== "" && t.html() !== "&nbsp;" || !!r.showEmptyCells) {
                        var i = t.index(),
                            s = "";
                        for (var o = 0; o < n.length; o++) {
                            o != 0 && (s += ": ");
                            var u = n[o][i];
                            s += u.text()
                        }
                        t.attr("data-th", s), r.contentWrap && !t.children().hasClass("bt-content") && t.wrapInner('<span class="bt-content" />')
                    } else t.addClass("bt-hide")
                })
            },
            i = function (t) {
                e.each(t.find("td"), function () {
                    var t = e(this),
                        n = t.children(".bt-content").html();
                    t.html(n)
                })
            },
            s = function (t, n) {
                n.forceResponsive ? e(window).width() <= n.breakpoint ? o(t, n) : u(t, n) : t.removeClass("bt").outerWidth() > t.parent().width() ? o(t, n) : u(t, n)
            },
            o = function (e, t) {
                e.addClass("bt"), t.tableWrap && e.parent(".bt-wrapper").addClass("adaptive-active")
            },
            u = function (e, t) {
                e.removeClass("bt"), t.tableWrap && e.parent(".bt-wrapper").removeClass("adaptive-active")
            },
            a = function (e, t) {
                e.find("td").removeAttr("data-th"), t.tableWrap && e.unwrap(), t.contentWrap && i(e), e.removeData("basictable")
            },
            f = function (e) {
                e.data("basictable") && s(e, e.data("basictable"))
            };
        this.each(function () {
            var r = e(this);
            if (r.length === 0 || r.data("basictable")) return r.data("basictable") && (t == "destroy" ? a(r, r.data("basictable")) : t === "start" ? o(r, r.data("basictable")) : t === "stop" ? u(r, r.data("basictable")) : s(r, r.data("basictable"))), !1;
            var i = e.extend({}, e.fn.basictable.defaults, t),
                l = {
                    breakpoint: i.breakpoint,
                    contentWrap: i.contentWrap,
                    forceResponsive: i.forceResponsive,
                    noResize: i.noResize,
                    tableWrap: i.tableWrap,
                    showEmptyCells: i.showEmptyCells
                };
            r.data("basictable", l), n(r, r.data("basictable")), l.noResize || (s(r, r.data("basictable")), e(window).bind("resize.basictable", function () {
                f(r)
            }))
        })
    }, e.fn.basictable.defaults = {
        breakpoint: 568,
        contentWrap: !0,
        forceResponsive: !0,
        noResize: !1,
        tableWrap: !1,
        showEmptyCells: !1
    }
})(jQuery);


/*!
 * headroom.js v0.9.3 - Give your page some headroom. Hide your header until you need it
 * Copyright (c) 2016 Nick Williams - http://wicky.nillia.ms/headroom.js
 * License: MIT
 */

! function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define([], b) : "object" == typeof exports ? module.exports = b() : a.Headroom = b()
}(this, function () {
    "use strict";

    function a(a) {
        this.callback = a, this.ticking = !1
    }

    function b(a) {
        return a && "undefined" != typeof window && (a === window || a.nodeType)
    }

    function c(a) {
        if (arguments.length <= 0) throw new Error("Missing arguments in extend function");
        var d, e, f = a || {};
        for (e = 1; e < arguments.length; e++) {
            var g = arguments[e] || {};
            for (d in g) "object" != typeof f[d] || b(f[d]) ? f[d] = f[d] || g[d] : f[d] = c(f[d], g[d])
        }
        return f
    }

    function d(a) {
        return a === Object(a) ? a : {
            down: a,
            up: a
        }
    }

    function e(a, b) {
        b = c(b, e.options), this.lastKnownScrollY = 0, this.elem = a, this.tolerance = d(b.tolerance), this.classes = b.classes, this.offset = b.offset, this.scroller = b.scroller, this.initialised = !1, this.onPin = b.onPin, this.onUnpin = b.onUnpin, this.onTop = b.onTop, this.onNotTop = b.onNotTop, this.onBottom = b.onBottom, this.onNotBottom = b.onNotBottom
    }
    var f = {
        bind: !! function () {}.bind,
        classList: "classList" in document.documentElement,
        rAF: !!(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame)
    };
    return window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame, a.prototype = {
        constructor: a,
        update: function () {
            this.callback && this.callback(), this.ticking = !1
        },
        requestTick: function () {
            this.ticking || (requestAnimationFrame(this.rafCallback || (this.rafCallback = this.update.bind(this))), this.ticking = !0)
        },
        handleEvent: function () {
            this.requestTick()
        }
    }, e.prototype = {
        constructor: e,
        init: function () {
            return e.cutsTheMustard ? (this.debouncer = new a(this.update.bind(this)), this.elem.classList.add(this.classes.initial), setTimeout(this.attachEvent.bind(this), 100), this) : void 0
        },
        destroy: function () {
            var a = this.classes;
            this.initialised = !1, this.elem.classList.remove(a.unpinned, a.pinned, a.top, a.notTop, a.initial), this.scroller.removeEventListener("scroll", this.debouncer, !1)
        },
        attachEvent: function () {
            this.initialised || (this.lastKnownScrollY = this.getScrollY(), this.initialised = !0, this.scroller.addEventListener("scroll", this.debouncer, !1), this.debouncer.handleEvent())
        },
        unpin: function () {
            var a = this.elem.classList,
                b = this.classes;
            !a.contains(b.pinned) && a.contains(b.unpinned) || (a.add(b.unpinned), a.remove(b.pinned), this.onUnpin && this.onUnpin.call(this))
        },
        pin: function () {
            var a = this.elem.classList,
                b = this.classes;
            a.contains(b.unpinned) && (a.remove(b.unpinned), a.add(b.pinned), this.onPin && this.onPin.call(this))
        },
        top: function () {
            var a = this.elem.classList,
                b = this.classes;
            a.contains(b.top) || (a.add(b.top), a.remove(b.notTop), this.onTop && this.onTop.call(this))
        },
        notTop: function () {
            var a = this.elem.classList,
                b = this.classes;
            a.contains(b.notTop) || (a.add(b.notTop), a.remove(b.top), this.onNotTop && this.onNotTop.call(this))
        },
        bottom: function () {
            var a = this.elem.classList,
                b = this.classes;
            a.contains(b.bottom) || (a.add(b.bottom), a.remove(b.notBottom), this.onBottom && this.onBottom.call(this))
        },
        notBottom: function () {
            var a = this.elem.classList,
                b = this.classes;
            a.contains(b.notBottom) || (a.add(b.notBottom), a.remove(b.bottom), this.onNotBottom && this.onNotBottom.call(this))
        },
        getScrollY: function () {
            return void 0 !== this.scroller.pageYOffset ? this.scroller.pageYOffset : void 0 !== this.scroller.scrollTop ? this.scroller.scrollTop : (document.documentElement || document.body.parentNode || document.body).scrollTop
        },
        getViewportHeight: function () {
            return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
        },
        getElementPhysicalHeight: function (a) {
            return Math.max(a.offsetHeight, a.clientHeight)
        },
        getScrollerPhysicalHeight: function () {
            return this.scroller === window || this.scroller === document.body ? this.getViewportHeight() : this.getElementPhysicalHeight(this.scroller)
        },
        getDocumentHeight: function () {
            var a = document.body,
                b = document.documentElement;
            return Math.max(a.scrollHeight, b.scrollHeight, a.offsetHeight, b.offsetHeight, a.clientHeight, b.clientHeight)
        },
        getElementHeight: function (a) {
            return Math.max(a.scrollHeight, a.offsetHeight, a.clientHeight)
        },
        getScrollerHeight: function () {
            return this.scroller === window || this.scroller === document.body ? this.getDocumentHeight() : this.getElementHeight(this.scroller)
        },
        isOutOfBounds: function (a) {
            var b = 0 > a,
                c = a + this.getScrollerPhysicalHeight() > this.getScrollerHeight();
            return b || c
        },
        toleranceExceeded: function (a, b) {
            return Math.abs(a - this.lastKnownScrollY) >= this.tolerance[b]
        },
        shouldUnpin: function (a, b) {
            var c = a > this.lastKnownScrollY,
                d = a >= this.offset;
            return c && d && b
        },
        shouldPin: function (a, b) {
            var c = a < this.lastKnownScrollY,
                d = a <= this.offset;
            return c && b || d
        },
        update: function () {
            var a = this.getScrollY(),
                b = a > this.lastKnownScrollY ? "down" : "up",
                c = this.toleranceExceeded(a, b);
            this.isOutOfBounds(a) || (a <= this.offset ? this.top() : this.notTop(), a + this.getViewportHeight() >= this.getScrollerHeight() ? this.bottom() : this.notBottom(), this.shouldUnpin(a, c) ? this.unpin() : this.shouldPin(a, c) && this.pin(), this.lastKnownScrollY = a)
        }
    }, e.options = {
        tolerance: {
            up: 0,
            down: 0
        },
        offset: 0,
        scroller: window,
        classes: {
            pinned: "headroom--pinned",
            unpinned: "headroom--unpinned",
            top: "headroom--top",
            notTop: "headroom--not-top",
            bottom: "headroom--bottom",
            notBottom: "headroom--not-bottom",
            initial: "headroom"
        }
    }, e.cutsTheMustard = "undefined" != typeof f && f.rAF && f.bind && f.classList, e
});

var options = {
    tolerance: {
        up: 0,
        down: 10
    }
}


$(function () {

    // оборачиваем сайт
    $("body").wrapInner("<div class='adaptive-wrap'>");

    // Создаем верхнюю панель
    // $('<div class="adaptive-top js-adaptive-panel" data-id="3">').insertAfter('.adaptive-wrap');

    // Создаем левую панель
    // $('<div class="adaptive-left js-adaptive-panel" data-id="0">').insertAfter('.adaptive-wrap');

    // создаем тьму
    // $('<div class="adaptive-darkness js-adaptive-darkness"></div>').insertAfter('.adaptive-wrap');

    // создаем закрывашку
    // var hider = $('<div class="adaptive-hide js-adaptive-trigger" data-back="hide" data-alignment="center"></div>');

    // вставляем закрывашку
    // hider.clone().prependTo('.adaptive-left');
    // hider.clone().prependTo('.adaptive-top');

    // Клонируем разметку в левое меню
    // $(".page-header").clone().prependTo('.adaptive-left');
    // $(".selector").clone().prependTo('.adaptive-left');
    //   $(".selector").removeClass('main-menu');

    // создаем форму
    // $('<form action="/search/">').appendTo('.adaptive-top');
    // $('<input type="text" name="q">').appendTo('.adaptive-top form');
    // $('<input type="submit" value="Найти">').appendTo('.adaptive-top form');

    // Создаем headroom
    // $('<div class="headroom" data-headroom>').insertAfter('.adaptive-wrap');
    // $('<div data-id="0" data-back="show" data-alignment="left" class="adaptive-call js-adaptive-trigger"></div>').appendTo('.headroom');
    // $('<a href="/" class="logo"><img src="./index_files/logotip_rezprom2.png" alt="" style="max-height: 40px;"></a>').appendTo('.headroom');
    // $('<div data-id="3" data-back="show" data-alignment="center" class="adaptive-call js-adaptive-trigger"></div>').appendTo('.headroom');

    // ловим хедрум
    var myElement = document.querySelector(".headroom");
    // Создаем экземпляр, передаем ему элемент
    var headroom = new Headroom(myElement, options);

    // инициализируем
    headroom.init();

    var body = $("body"); //-------------------------- body
    var adaptiveTrigger = $('.js-adaptive-trigger'); //---------- Переключатель состояния
    var adaptiveBack = $('.js-adaptive-darkness'); //--------- Активная подложка-затемнение
    var adaptivePanel = $('.js-adaptive-panel'); //------------ Выезжающая панель
    var adaptiveCloneForPanel = $('.js-adaptive-clone-for-panel'); //-- Элементы для размещения в левой панели

    // Обработка клика на переключатель.
    adaptiveTrigger.on('click', function (event) {
        event.preventDefault();
        var $this = $(this);
        adaptiveTrigger.removeClass('adaptive-active');
        $this.addClass('adaptive-active');
        // body.attr("data-alignment", $this.data("alignment"));
        adaptiveBack.attr("data-state", $this.data('back'));
        adaptivePanel.removeClass('adaptive-active').filter("[data-id='" + $this.data("id") + "']").addClass('adaptive-active');
    });

    adaptiveBack.on('click', function (event) {
        adaptiveTrigger.removeClass('adaptive-active');
        adaptiveBack.attr("data-state", "hide");
        // body.attr("data-alignment", "center");
        adaptivePanel.removeClass('adaptive-active');
    });

    adaptiveCloneForPanel.each(function (index, el) {
        var $this = $(el);
        adaptivePanel.filter("[data-id='" + $this.data("id") + "']").append($this.clone());
    });

    var adaptiveMenuTrigger = $("<span class='js-adaptive-menu-trigger'>");

    adaptivePanel.find('ul').each(function (index, el) {
        var $this = $(this);
        $this.find('a,span,ul,li,div').removeAttr('class');
        $this.closest('li').addClass('has-child').find('>a').append(adaptiveMenuTrigger.clone());
    });

    adaptivePanel.find('.js-adaptive-menu-trigger').on('click', function (event) {
        event.preventDefault();
        $(this).toggleClass('adaptive-active').closest("li").find('>ul').toggleClass('adaptive-active');
    });

    // adaptiveCloneForHeader.clone().prependTo('.adaptive-header');

    $("#content").find("table").not(".dont-wrap").wrap("<div class='adaptive-table'>");

    $('.adaptive-wrap .side-menu .zag').on('click', function(){
        $(this).toggleClass('active');
        $(this).next('ul').slideToggle();
    })

    $('.adaptive-wrap .second-click').on('click', function(){
        $(this).toggleClass('active');
        $(this).prev('ul').slideToggle();
    })
    
    function maxTd( table ) {
        var maxCount = 0;
        $(table).find('tr').each(function(){
            if (maxCount < $(this).find('td').length) {
                maxCount = $(this).find('td').length;
            };
        });
        return maxCount;
    };

    $('.tableToggle').each(function(){
        $(this).append('<tr class="tableToggleRow"><td colspan="' + maxTd( $(this) ) + '"><a class="tableToggleBtn"><span class="textHidden">Показать все</span><span class="textShown" style="display: none;">Скрыть</span></a></td></tr>')
        $(this).find('tr').eq(6).nextAll(':not(.tableToggleRow)').hide();
    });

    function tableToggle (table) {
        $(table).find('tr').eq(6).nextAll(':not(.tableToggleRow)').toggle();
    };

    $('.tableToggleBtn').click(function (e) { 
        e.preventDefault();
        tableToggle($(this).parents('.tableToggle'));
        $(this).find('span').toggle();
    });
    
});

var searchElem = $('.header-wrapper .search-wrapper');
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
var moveSearch = debounce(function() {
    if ($(window).width() <= 992) {
        if ($('.adaptive-top .search-wrapper').length == 0) {
            $('.header-wrapper .search-wrapper').remove();
            searchElem.appendTo($('.adaptive-top'))
        }

        $('.adaptive-wrap .side-menu > ul > li').each(function(){
            if ($(this).find('ul').length) {
                $(this).append('<span class="second-click"></span>');
                $(this).find('ul').hide();
            }
        });

        $('.RealTable').each(function(){
            // console.log(this);
            $(this).addClass('adaptive-table').wrap('<div class="bt-wrapper"></div>');
        });

    }
    else if ($(window).width() > 992) {
        if ($('.header-wrapper .search-wrapper').length == 0) {
            $('.adaptive-top .search-wrapper').remove();
            searchElem.appendTo($('.header-wrapper'))
        }


        $('.bt-wrapper').each(function(){
            $(this).addClass('bt-wrapper');
            $(this).find('.adaptive-table').removeClass('.adaptive-table');
        });

        $('.adaptive-wrap .side-menu > ul > li').each(function(){
            if ($(this).find('ul').length) {
                $(this).find('.second-click').remove();
                $(this).find('ul').removeAttr('style');
            }
        });
    }
}, 250);

window.addEventListener('resize', moveSearch);
window.addEventListener('load', moveSearch);