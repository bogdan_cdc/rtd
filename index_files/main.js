//map
var map;
function initialize() {
	var mapOptions = {
		zoom: 8,
		center: new google.maps.LatLng(-34.397, 150.644),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('gmap'),
		mapOptions);
}

// google.maps.event.addDomListener(window, 'load', initialize);

// $('.accordion .hdr span').click(function(){
// 	$('.accordion .acc-content').stop(true, true).slideUp(100);
// 	$(this).closest('.hdr').next().stop(true, true).slideDown(100);
// });


$('.accordion .hdr span').click(function(){
	$('.accordion .acc-content').stop(true, true).slideUp(100);
	$(this).closest('.hdr').next().stop(true, true).slideDown(100);
});

// $('.catalog-slider ul').carouFredSel({
// 	prev:'#catalog-slider-prev',
// 	next:'#catalog-slider-next',
// 	swipe:{
// 		onTouch:true,
// 		onMouse:true
// 	},
// 	auto:false,
// 	circular:false,
// 	scroll:1
// });


$(document).ready(function(){


	$('.hdr').click(function(){

		$('.hdr').removeClass('active_vk');
		$(this).addClass('active_vk');
		var ids = $(this).attr('id') + 'f';
		$('.obv .acc-content').css({'display':'none'});
		$('.' + ids).css({'display':'block'});
	});

	// $('.bxslider-main').bxSlider({
	// 	auto: true,
	// 	autoHover: true,
    // });

    var swiper = new Swiper('._sw_slider-1', {
        // spaceBetween: 30,
        // pagination: {
        //     el: '.swiper-pagination',
        //     clickable: true,
        // },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var prod_swiper = new Swiper('.prod_slider', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 50,
            },
        }
    });
    var prod_swiper2 = new Swiper('.prod_slider2', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 4,
            },
            1024: {
                slidesPerView: 5,
            },
        }
    });
    var prod_swiper3 = new Swiper('.prod_slider3', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 3,
            },
        }
    });


	$('.bxslider').bxSlider();

	// $('.bxslider2').bxSlider({
	// 	//minSlides: 3,
	// 	maxSlides: 3,
	// 	slideWidth: 360,
	// 	slideMargin: 10
	// });

	// var sli3 = $('.bxslider3').bxSlider({
	// 	minSlides: 4,
	// 	maxSlides: 4,
	// 	slideWidth: 360,
	// 	slideMargin: 10
    // });
    




	$('.bxslider2').parent('.bx-viewport').addClass('he');

	// wtf??? $('.shirZakaz .b_red, .cnopck .ch1').click();


	$(document).on('click','.prosm .zak', function(){
		$('.prosm').fadeOut().remove();
		$('.overgal').fadeOut();
	});

/*
	$('.photo img').click(function(){
		var hh = $(document).height();
		$('.overgal').css({'height':hh+'px'}).fadeIn();

		// $(this).parent('.p').addClass('prosm');

		$('.overgal, .prosm .zak').click(function(){
			$(this).fadeOut();
			// $('.photo .p').removeClass('prosm');
			$('.prosm').fadeOut().remove();
		});

		var src = $(this).attr('date-src');
		$(this).parents('.wr').prepend("<div class='prosm'><div class='zak'></div><img src='" + src + "'></div>");
	});
*/


	$('.photo .photo_butt').click(function(){


		$(this).toggleClass('trig_but');
		
		

		if(this.classList.contains('trig_but')){

			//отключаем слайдер
			sli3.destroySlider();
			
			$(this).html('Скрыть галерею');
			// $(this).html('Смотреть все фото');

			
		}else{
			$(this).html('Смотреть все фото');
			sli3 = $('.bxslider3').bxSlider({
				minSlides: 4,
				maxSlides: 4,
				slideWidth: 360,
				slideMargin: 10
			});
		}

	});

});

$(window).scroll(function(){
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if (width <= 600) return;
    ($(document).scrollTop() > 350) ? $('.header-moving').show() : $('.header-moving').hide();
});
